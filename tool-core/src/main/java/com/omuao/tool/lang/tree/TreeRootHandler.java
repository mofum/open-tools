package com.omuao.tool.lang.tree;

public interface TreeRootHandler {

    Object rootTag();

    static TreeRootHandler build(Object value) {
        return () -> value;
    }

    static TreeRootHandler defaultHandler() {
        return build(null);
    }
}
