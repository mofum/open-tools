package com.omuao.tool.lang.tree.anotation;

import com.omuao.tool.lang.tree.TreeRootHandler;

import java.lang.annotation.*;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
@Inherited
public @interface TreePid {
    Class<? extends TreeRootHandler> rootTag() default TreeRootHandler.class;

}
