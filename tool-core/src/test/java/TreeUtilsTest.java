import com.omuao.tool.lang.tree.TreeRootHandler;
import com.omuao.tool.lang.tree.TreeUtils;
import com.omuao.tool.lang.tree.anotation.TreeChildren;
import com.omuao.tool.lang.tree.anotation.TreeId;
import com.omuao.tool.lang.tree.anotation.TreePid;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TreeUtilsTest {

    @Test
    public void simpleTreeTest() {
        List<CategoryExtensions> extensions = TreeUtils.findAnnotationTreeRoots(Category.selectExamples(), CategoryExtensions.class, 0L);
        Assert.assertEquals(extensions.size(), 4);
    }

    @Test
    public void subTreeTest() {
        List<CategoryExtensions> extensions = TreeUtils.findAnnotationTreeRoots(Category.selectExamples(), CategoryExtensions.class, 1L);
        Assert.assertEquals(extensions.size(), 5);
    }

    @Test
    public void treeTest2() {
        List<CategoryExtensions2> extensions = TreeUtils.findAnnotationTreeRoots(Category.selectExamples(), CategoryExtensions2.class, 0L);
        Assert.assertEquals(extensions.size(), 4);
    }

    @Test
    public void subTreeTest2() {
        List<CategoryExtensions2> extensions = TreeUtils.findAnnotationTreeRoots(Category.selectExamples(), CategoryExtensions2.class, 1L);
        Assert.assertEquals(extensions.size(), 5);
    }

    @Test
    public void annotationValueOf() {
        List<TreeUtils.TreeNode<Category>> categories2 = TreeUtils.annotationValueOf(Category.selectExamples(), Category.class);
        Assert.assertEquals(categories2.size(), Category.selectExamples().size());
    }

    @Test
    public void treeTestDefault() {
        List<CategoryExtensions2> extensions = TreeUtils.findAnnotationTreeRoots(Category.selectExamples(), CategoryExtensions2.class);
        Assert.assertEquals(extensions.size(), 0);
    }

    @Test
    public void treeTestInterfaceDefault() {
        List<CategoryExtensions> extensions = TreeUtils.findAnnotationTreeRoots(Category.selectExamples(), CategoryExtensions.class);
        Assert.assertEquals(extensions.size(), 4);
    }

    @Test
    public void treeTest() {
        List<CategoryExtensions> extensions = TreeUtils.findAnnotationTreeRoots(Category.selectExamples(), CategoryExtensions.class);
        Assert.assertEquals(extensions.size(), 4);
    }

    @Test
    public void simpleTree3Test() {
        List<CategoryExtensions3> extensions = TreeUtils.findAnnotationTreeRoots(CategoryExtensions3.selectExamples(), CategoryExtensions3.class, 0L);
        Assert.assertEquals(extensions.size(), 4);
    }

    public static class CategoryExtensions3 {
        @TreeId
        private Long categoryId;
        @TreePid
        private Long categoryPid;
        @TreeChildren
        private List<CategoryExtensions> categoryChildren;

        private String categoryName;

        public CategoryExtensions3() {
        }

        public CategoryExtensions3(Long categoryId, Long categoryPid, String categoryName) {
            this.categoryId = categoryId;
            this.categoryPid = categoryPid;
            this.categoryName = categoryName;
        }

        public Long getCategoryId() {
            return categoryId;
        }

        public void setCategoryId(Long categoryId) {
            this.categoryId = categoryId;
        }

        public Long getCategoryPid() {
            return categoryPid;
        }

        public void setCategoryPid(Long categoryPid) {
            this.categoryPid = categoryPid;
        }

        public List<CategoryExtensions> getCategoryChildren() {
            return categoryChildren;
        }

        public void setCategoryChildren(List<CategoryExtensions> categoryChildren) {
            this.categoryChildren = categoryChildren;
        }

        public static List<CategoryExtensions3> selectExamples() {
            List<CategoryExtensions3> data = new ArrayList<>();
            data.add(new CategoryExtensions3(1L, 0L, "测试1"));
            data.add(new CategoryExtensions3(2L, 0L, "测试2"));
            data.add(new CategoryExtensions3(3L, 0L, "测试3"));
            data.add(new CategoryExtensions3(4L, 0L, "测试4"));
            data.add(new CategoryExtensions3(5L, 1L, "测试5"));
            data.add(new CategoryExtensions3(6L, 1L, "测试6"));
            data.add(new CategoryExtensions3(7L, 1L, "测试7"));
            data.add(new CategoryExtensions3(8L, 1L, "测试8"));
            data.add(new CategoryExtensions3(9L, 1L, "测试9"));
            data.add(new CategoryExtensions3(10L, 2L, "测试10"));
            data.add(new CategoryExtensions3(11L, 2L, "测试11"));
            data.add(new CategoryExtensions3(12L, 2L, "测试12"));
            data.add(new CategoryExtensions3(13L, 2L, "测试13"));
            data.add(new CategoryExtensions3(14L, 2L, "测试14"));
            data.add(new CategoryExtensions3(15L, 8L, "测试15"));
            data.add(new CategoryExtensions3(16L, 8L, "测试16"));
            data.add(new CategoryExtensions3(17L, 8L, "测试17"));
            data.add(new CategoryExtensions3(18L, 8L, "测试18"));
            data.add(new CategoryExtensions3(19L, 8L, "测试19"));
            return data;
        }
    }

    public static class CategoryExtensions2 {
        @TreeId
        private Long id;
        @TreePid
        private Long parentId;
        @TreeChildren
        private List<CategoryExtensions> children;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getParentId() {
            return parentId;
        }

        public void setParentId(Long parentId) {
            this.parentId = parentId;
        }

        public List<CategoryExtensions> getChildren() {
            return children;
        }

        public void setChildren(List<CategoryExtensions> children) {
            this.children = children;
        }
    }

    /**
     * 示例代码
     */
    public static class CategoryExtensions extends Category implements TreeRootHandler {
        @TreeChildren
        private List<CategoryExtensions> children;

        @Override
        public Object rootTag() {
            return 0L;
        }

        public List<CategoryExtensions> getChildren() {
            return children;
        }

        public void setChildren(List<CategoryExtensions> children) {
            this.children = children;
        }
    }

    /**
     * 示例代码
     */
    public static class Category {
        @TreeId
        private Long id;
        @TreePid
        private Long parentId;

        public Category() {
        }

        public Category(Long id, Long parentId) {
            this.id = id;
            this.parentId = parentId;
        }

        /**
         * 模拟数据库查询的数据
         *
         * @return
         */
        public static List<Category> selectExamples() {
            List<Category> data = new ArrayList<>();
            data.add(new Category(1L, 0L));
            data.add(new Category(2L, 0L));
            data.add(new Category(3L, 0L));
            data.add(new Category(4L, 0L));
            data.add(new Category(5L, 1L));
            data.add(new Category(6L, 1L));
            data.add(new Category(7L, 1L));
            data.add(new Category(8L, 1L));
            data.add(new Category(9L, 1L));
            data.add(new Category(10L, 2L));
            data.add(new Category(11L, 2L));
            data.add(new Category(12L, 2L));
            data.add(new Category(13L, 2L));
            data.add(new Category(14L, 2L));
            data.add(new Category(15L, 8L));
            data.add(new Category(16L, 8L));
            data.add(new Category(17L, 8L));
            data.add(new Category(18L, 8L));
            data.add(new Category(19L, 8L));
            return data;
        }

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public Long getParentId() {
            return parentId;
        }

        public void setParentId(Long parentId) {
            this.parentId = parentId;
        }
    }
}
