让我们用更简洁方式构建树

原始数据
~~~
    @Data
    public static class CategoryExtensions extends Category{
        @TreeChildren
        private List<CategoryExtensions> children;
    }

    @Data
    @NoArgsConstructor
    public static class Category {
        @TreeId
        private Long id;
        @TreePid
        private Long parentId;

        public Category(Long id, Long parentId) {
            this.id = id;
            this.parentId = parentId;
        }

        /**
         * 模拟数据库查询的数据
         *
         * @return
         */
        public static List<Category> selectExamples() {
            List<Category> data = new ArrayList<>();
            data.add(new Category(1L, null));
            data.add(new Category(2L, null));
            data.add(new Category(3L, null));
            data.add(new Category(4L, null));
            data.add(new Category(5L, 1L));
            data.add(new Category(6L, 1L));
            data.add(new Category(7L, 1L));
            data.add(new Category(8L, 1L));
            data.add(new Category(9L, 1L));
            data.add(new Category(10L, 2L));
            data.add(new Category(11L, 2L));
            data.add(new Category(12L, 2L));
            data.add(new Category(13L, 2L));
            data.add(new Category(14L, 2L));
            data.add(new Category(15L, 8L));
            data.add(new Category(16L, 8L));
            data.add(new Category(17L, 8L));
            data.add(new Category(18L, 8L));
            data.add(new Category(19L, 8L));
            return data;
        }
    }
~~~
使用方式：
~~~
List<CategoryExtensions> extensions = TreeUtils.findAnnotationTreeRoots(Category.selectExamples(), CategoryExtensions.class);
~~~
